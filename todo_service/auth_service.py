from abc import ABC
from calendar import timegm
from datetime import datetime, timedelta
from fastapi import (
    Cookie,
    Depends,
    HTTPException,
    Request,
    status,
)
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from jose.constants import ALGORITHMS
from typing import Optional
import bcrypt

import os

from queries.user_queries import UserOut


SIGNING_KEY = os.environ.get("SIGNING_KEY")

class AuthService(ABC):
    def __init__(
        self,
        algorithm: str = ALGORITHMS.HS256,
    ):
        self.algorithm = algorithm
        self.scheme = OAuth2PasswordBearer(tokenUrl="token", auto_error=False)
        self.cookie_name = "fastapi_token"
        self.salt = bcrypt.gensalt()

        """
        gets token from cookie or bearer token
        decodes
        fails silently
        """
        async def decode_jwt(
            self,
            bearer_token: Optional[str] = Depends(self.scheme),
            cookie_token: Optional[str] = Cookie(default=None, alias=self.cookie_name),
        ):
            token = cookie_token
            if not token and bearer_token:
                token = bearer_token
            try:
                payload = jwt.decode(token, SIGNING_KEY, algorithms=[algorithm])
                return payload
            except (JWTError, AttributeError):
                pass
            return None

        setattr(
            self,
            "decode_jwt",
            decode_jwt.__get__(self, self.__class__),
        )

        """
        gets user from token
        fails silently
        """
        async def try_get_current_user_data(
            self,
            token = Depends(self.decode_jwt),
        ):
            if token and "user" in token:
                return token["user"]
            return None

        setattr(
            self,
            "try_get_current_user_data",
            try_get_current_user_data.__get__(self, self.__class__),
        )

        """
        gets user from token
        fails loudly
        """
        async def get_current_user_data(
            self,
            data = Depends(self.try_get_current_user_data),
        ) -> Optional[dict]:
            if data is None:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Invalid token",
                    headers={"WWW-Authenticate": "Bearer"},
                )
            return data

        setattr(
            self,
            "get_current_user_data",
            get_current_user_data.__get__(self, self.__class__),
        )

    def hash_password(self, plain_password) -> str:
        return bcrypt.hashpw(plain_password.encode('utf-8'), self.salt).decode()

    def verify_password(self, plain_password, hashed_password) -> bool:
        if bcrypt.checkpw(plain_password.encode('utf-8'), hashed_password.encode('utf-8')):
            return True
        else:
            raise Exception('passwords do not match')

    def get_cookie_settings(self, request: Request):
        samesite = "lax"
        secure = True
        headers = request.headers
        if "origin" in headers and "localhost" in headers["origin"]:
            secure = False
        return samesite, secure

    def get_jwt(self, user_out: UserOut):
        exp = timegm((datetime.utcnow() + timedelta(hours=1)).utctimetuple())
        jwt_data = {"exp": exp, "sub": user_out.username, "user": user_out.dict()}
        encoded_jwt = jwt.encode(jwt_data, SIGNING_KEY, algorithm=self.algorithm)
        return encoded_jwt

auth_service = AuthService()
