from fastapi import APIRouter, Depends
from queries.todo_queries import TodoQueries, TodoIn
from auth_service import auth_service


router = APIRouter()

@router.get("/api/todos")
def get_all(
    queries: TodoQueries = Depends(),
    user: dict = Depends(auth_service.try_get_current_user_data),
):
    if user:
        return queries.get_all_by_user_id(user["id"])
    return queries.get_all()

@router.get("/api/todos/{id}")
def get_by_id(
    id: int,
    queries: TodoQueries = Depends()
):
    return queries.get_by_id(id)

@router.post("/api/todos")
def create_todo(
    todo: TodoIn,
    queries: TodoQueries = Depends(),
    user: dict = Depends(auth_service.get_current_user_data),
):
    return queries.create_todo(todo, user['id'])
