from fastapi import Depends, Request, Response, HTTPException, status, APIRouter
from queries.user_queries import UserQueries, UserIn, UserOut
from auth_service import auth_service


router = APIRouter()

# signup (also signs in)
@router.post("/signup")
async def signup(
    new_user: UserIn,
    request: Request,
    response: Response,
    queries: UserQueries = Depends(),
) -> UserOut:
    hashed_password = auth_service.hash_password(new_user.password)
    user = queries.create_user(new_user.username, hashed_password)
    user_out = UserOut(**user.dict())
    token = auth_service.get_jwt(user_out)
    samesite, secure = auth_service.get_cookie_settings(request)
    response.set_cookie(
        key=auth_service.cookie_name,
        value=token,
        httponly=True,
        samesite=samesite,
        secure=secure,
    )
    return user_out

# signin
@router.post("/signin")
async def signin(
    user_data: UserIn,
    request: Request,
    response: Response,
    queries: UserQueries = Depends(),
) -> UserOut:
    try:
        user = queries.get_by_username(user_data.username)
        auth_service.verify_password(user_data.password, user.password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    user_out = UserOut(**user.dict())
    token = auth_service.get_jwt(user_out)
    samesite, secure = auth_service.get_cookie_settings(request)
    response.set_cookie(
        key=auth_service.cookie_name,
        value=token,
        httponly=True,
        samesite=samesite,
        secure=secure,
    )
    return user_out

# refresh
@router.get("/current")
async def get_current(
    request: Request,
    user: dict = Depends(auth_service.try_get_current_user_data)
) -> UserOut:
    if user and auth_service.cookie_name in request.cookies:
        return user

# signout
@router.delete("/signout")
async def signout(
    request: Request,
    response: Response,
) -> dict:
    samesite, secure = auth_service.get_cookie_settings(request)
    response.delete_cookie(
        key=auth_service.cookie_name,
        httponly=True,
        samesite=samesite,
        secure=secure,
    )
    return True