import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class TodoIn(BaseModel):
    action: str
    description: str
    value: int|str

class TodoOut(BaseModel):
    id: int
    action: str
    description: str
    value: int
    user_id: int

def sanitize_todos(results: list[tuple]) -> list[TodoOut]:
    return [TodoOut(
        id=result[0],
        action=result[1],
        description=result[2],
        value=result[3],
        user_id=result[4],
    ) for result in results]

class TodoQueries:
    def get_all(self) -> list[TodoOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM todos
                    """
                )
                results = cur.fetchall()
                return sanitize_todos(results)

    def get_all_by_user_id(self, user_id: int) -> list[TodoOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM todos
                    WHERE user_id = %s
                    """,
                    [user_id]
                )
                results = cur.fetchall()
                return sanitize_todos(results)

    def get_by_id(self, id: int) -> TodoOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        id,
                        action,
                        description,
                        value,
                        user_id
                    FROM todos
                    WHERE id = %s
                    """,
                    [id]
                )
                result = cur.fetchone()
                return sanitize_todos([result])[0]

    def create_todo(self, todo: TodoIn, user_id: int) -> TodoOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    INSERT INTO todos (
                        action,
                        description,
                        value,
                        user_id
                    ) VALUES (
                        %s, %s, %s, %s
                    )
                    RETURNING id, action, description, value, user_id;
                    """,
                    [
                        todo.action,
                        todo.description,
                        todo.value,
                        user_id,
                    ]
                )
                result = cur.fetchone()
                return sanitize_todos([result])[0]
