import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { useAuthContext } from "./authService";

function TodoList() {
  const [todos, setTodos] = useState([]);
  const { user } = useAuthContext();
  const navigate = useNavigate();

  useEffect(() => {
    if (!user?.id) {
      navigate("/signin");
    }
  }, [user]);

  useEffect(() => {
    const getTodos = async () => {
      const res = await fetch(
        `${process.env.REACT_APP_TODO_SERVICE_API_HOST}/api/todos`,
        {
          method: "get",
          credentials: "include",
        }
      );
      const results = await res.json();
      setTodos(results);
    };
    getTodos();
  }, []);

  return (
    <div>
      <ul>
        {todos.map((t) => (
          <li key={t.id}>{t.action}</li>
        ))}
      </ul>
    </div>
  );
}

export default TodoList;
