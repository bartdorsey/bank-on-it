import useAuthService from "./authService";
import { Routes, Route, NavLink } from "react-router-dom";

import Auth from "./Auth";
import TodoList from "./TodoList";
import TodoForm from "./TodoForm";

import "./App.css";

function App() {
  const { signout, user } = useAuthService();

  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          width: "80%",
          margin: "auto",
        }}
      >
        <NavLink to="/create-todo">new todo</NavLink>
        <NavLink to="my-list">my list</NavLink>
        {user ? (
          <button onClick={signout}>sign out</button>
        ) : (
          <>
            <NavLink to="/signin">sign in</NavLink>
            <NavLink to="/signup">sign up</NavLink>
          </>
        )}
      </div>
      {user && <div>welcome user: {user.username}</div>}
      <Routes>
        <Route path="/" element={<Auth />} />
        <Route path="/signup" element={<Auth />} />
        <Route path="/signin" element={<Auth />} />
        <Route path="/create-todo" element={<TodoForm />} />
        <Route path="/my-list" element={<TodoList />} />
      </Routes>
    </div>
  );
}

export default App;
